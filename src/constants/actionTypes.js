export const ENTER_KEY = 13;

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const REGISTER = 'REGISTER';
export const SUCCES_REGISTER = 'SUCCES_REGISTER';
export const LOGIN_STATUS = 'LOGIN_STATUS';
export const INSERT_TOKEN = 'INSERT_TOKEN';

export const APP_LOAD = 'APP_LOAD';
export const REDIRECT = 'REDIRECT';

export const UPDATE_FIELD_AUTH = 'UPDATE_FIELD_AUTH';
export const LOGIN_PAGE_UNLOADED = 'LOGIN_PAGE_UNLOADED';
export const REGISTER_PAGE_UNLOADED = 'REGISTER_PAGE_UNLOADED';

export const REQUEST_ABOUT = 'REQUEST_ABOUT';

export const ASYNC_START = 'ASYNC_START';
export const ASYNC_END = 'ASYNC_END';

export const GET_USERS = 'GET_USERS';
export const GET_MESSAGES = 'GET_MESSAGES';
