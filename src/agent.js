import axios from 'axios';

const API_ROOT = 'http://localhost:3040/';
// const API_ROOT = 'https://crawler-r.herokuapp.com/';

let token = null;

const config = () => {
  return {
    headers: {
      'Authorization': "Bearer " + token,
      "Content-Type": "application/json",
    }
  }
}

const requests = {
  del: url => axios.del(`${API_ROOT}${url}`),
  get: (url) => axios.get(`${API_ROOT}${url}`, config()),

  put: (url, body) => axios.put(`${API_ROOT}${url}`, body, config()),
  post: (url, body) => axios.post(`${API_ROOT}${url}`, body, config())
};

const Auth = {
  login: (email, password) =>
    requests.post('authentication/', { strategy: "local", email, password }),

  register: (username, email, password) => requests.post('users/', { username, email, password }),

  getAbout: (callBack) =>
    requests.get('about', callBack)
};

const Users = {
  getAll: () => requests.get('getallusers/')
}

const Messages = {
  get: () => requests.get('messages/')
}

export default {
  Auth,
  Users,
  Messages,
  setToken: _token => { token = _token; }
};