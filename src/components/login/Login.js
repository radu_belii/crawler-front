import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import { connect } from 'react-redux';
import { EMAIL_REGEX } from '../../shared/sharedHelpers';

import agent from '../../agent';

import {
  UPDATE_FIELD_AUTH,
  LOGIN,
  LOGIN_PAGE_UNLOADED,
  ENTER_KEY,
} from '../../constants/actionTypes'

const styles = theme => ({
  wrapper: {
    position: 'relative',
  },
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -2,
    marginLeft: -12,
  },
  buttonFail: {
    backgroundColor: red[500],
    '&:hover': {
      backgroundColor: red[700],
    },
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
});

const mapStateToProps = state => ({ ...state.auth });

const mapDispatchToProps = dispatch => ({
  onChangeEmail: value =>
    dispatch({ type: UPDATE_FIELD_AUTH, key: 'email', value }),
  onChangePassword: value =>
    dispatch({ type: UPDATE_FIELD_AUTH, key: 'password', value }),

  onSubmit: (email, password) =>
    dispatch({ type: LOGIN, payload: agent.Auth.login(email, password) }),
  onUnload: () =>
    dispatch({ type: LOGIN_PAGE_UNLOADED }),
});

class SignIn extends Component {
  constructor() {
    super();

    this.state = {
      isMailError: false,
    }

    this.changeEmail = ev => {
      if (this.state.isMailError) {
        this.setState({ isMailError: false })
      }
      this.props.onChangeEmail(ev.target.value)
    };

    this.changePassword = ev => this.props.onChangePassword(ev.target.value);

    this.submitForm = (email, password) => ev => {
      ev.preventDefault();

      if (email && password) {
        this.props.onSubmit(email, password);
      }

      if (ev.charCode === ENTER_KEY && (email || password) ) {
        this.props.onSubmit(email, password);
      }

      if (!EMAIL_REGEX.test(email)) {
        this.setState({ isMailError: true });
        return;
      };

    };
  }

  // after destroy component will erase all data in auth store
  componentWillUnmount() {
    this.props.onUnload();
  }
  render() {
    // retrive info from state 
    const email = this.props.email;
    const password = this.props.password;

    return (
      <main className={this.props.classes.main}>
        <CssBaseline />
        <Paper className={this.props.classes.paper}>
          <Avatar className={this.props.classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
        </Typography>

          <form className={this.props.classes.form}
            onKeyPress={(ev) => {
              if (ev.charCode === ENTER_KEY) {
                this.submitForm(email, password)
              }
            }} >
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">{this.state.isMailError ? 'Email is not valid' : 'Email Address'}  </InputLabel>
              <Input
                error={this.state.isMailError}
                id="outlined-error"
                label="Error"
                defaultValue={email}
                onChange={this.changeEmail}
                name="email"
                autoComplete="email"
                autoFocus
                required />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                defaultValue={password}
                onChange={this.changePassword}
                name="password"
                type="password"
                autoComplete="current-password"
                required />
            </FormControl>
            <div className={this.props.classes.wrapper}>
              <Button
                onClick={this.submitForm(email, password)}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={this.props.error ? this.props.classes.buttonFail : this.props.classes.submit}
                disabled={this.props.inProgress}
              >
                {this.props.error ? 'Faild Sign in' : 'Sign in'}
              </Button>
              {this.props.inProgress && <CircularProgress size={24} className={this.props.classes.buttonProgress} />}
            </div>
          </form>
        </Paper>
      </main>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SignIn));
