import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import agent from '../../agent';
import { connect } from 'react-redux';
import InfoCard from './../shared/InfoCard';
import Loading from './../Loading';

import {
  GET_USERS,
  GET_MESSAGES,
} from './../../constants/actionTypes';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

const mapStateToProps = state => ({ ...state.users });

const mapDispatchToProps = dispatch => ({

  onGetUsers: () =>
    dispatch({ type: GET_USERS, payload: agent.Users.getAll() }),
  onGetMessages: () =>
    dispatch({ type: GET_MESSAGES, payload: agent.Messages.get() })
});


class ControlPanel extends Component {

  render() {
    const { messages, inProgress } = this.props;
    return (
      <>


        {/* <Button variant="contained" color="primary"
          onClick={this.props.onGetUsers}
        >
          get all Users
      </Button> */}

        <Button variant="contained" color="primary"
          onClick={this.props.onGetMessages}
        >
          get Messages
      </Button>
        
        {inProgress && <Loading />}
        {messages && <InfoCard messages={messages} />}
      </>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ControlPanel));