import React, { Component } from 'react';
import NotLog from './NotLog';
import MainView from './MainView';

import { connect } from 'react-redux';


const mapStateToProps = state => ({
  token: state.common.token
});

const mapDispatchToProps = dispatch => ({
  onTabClick: () => dispatch({})
});
class Home extends Component {

  render() {
    const { token } = this.props;
    return (
      <>
        {!token && <NotLog />}
        {token && <MainView />}
      </>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);