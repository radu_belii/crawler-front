import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import withStyles from '@material-ui/core/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  card: {
    maxWidth: 345,
    marginTop: 20,
  },
  media: {
    height: 140,
  },
});

class NotLog extends Component {

  render() {
    return (
      <article className={this.props.classes.main}>
        <CssBaseline />
        <Card className={this.props.classes.card}>
          <CardActionArea>
            <CardMedia
              className={this.props.classes.media}
              image="/static/images/cards/contemplative-reptile.jpg"
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Crawler welcome
          </Typography>
              <Typography component="p">
                You dont have any credentials to acces the service so
                Resister on Log into your account
          </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Link
              to="/login">
              <Button size="small" color="primary">
                Log In
        </Button>
            </Link>
            <Link to="/register">

              <Button size="small" color="primary">
                Sign Up
        </Button>
            </Link>
          </CardActions>
        </Card>
      </article>
    );
  }


}

export default withStyles(styles)(NotLog);

