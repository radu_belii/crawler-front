import React from 'react';
import ControlPanel from './ControlPanel';

const MainView = (props) => (

    <div>
        {!props.token && 'You are now logged and can grab Your crowl data '}
        {<ControlPanel />}
    </div>)

export default MainView;