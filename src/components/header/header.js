import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import MenuOptions from './MenuOptions';
import { Link } from 'react-router-dom';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  aTag: {
    color: '#fff',
    textDecoration: 'none'
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class Header extends Component {
  render() {
    const { classes, token } = this.props;
    const loginRegister =
      <>
        <Link to="/login" className={classes.aTag}>
          <Button color="inherit">Login</Button>
        </Link>

        <Link to="/register" className={classes.aTag}>
          <Button color="inherit">Register</Button>
        </Link>
      </>

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              <Link className={classes.aTag}
                to="/">
                Crawler
            </Link>
            </Typography>
            <Typography>
              {token && 'Welcome User'}
            </Typography>
            {!token && loginRegister}
            {token && <MenuOptions />}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(styles)(Header);