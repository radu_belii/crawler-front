import React, { Component } from 'react';
import SignIn from './login/Login.js';
import Home from './home/index.js';
import Register from './Register';
import Header from './header/Header';
import agent from '../agent';
import { Route, Switch } from 'react-router-dom';
import { store } from '../redux/store';
import { push } from 'react-router-redux';

import { connect } from 'react-redux';
import './App.scss';

import Loading from './Loading';
import { 
  APP_LOAD,
  REDIRECT,
} from '../constants/actionTypes';

const mapStateToProps = state => {
  return {
    // inProgress: state.vehicles.inProgress,
    appLoaded: state.common.appLoaded,
    redirectTo: state.common.redirectTo,
    token: state.common.token 
  }
};

const mapDispatchToProps = dispatch => ({
  // onFetchAllVehicles: () =>
  //   dispatch({ type: LOAD_VEHICLES, payload: agent.Vehicle.getAll() }),
  onLoad: (token) => 
    dispatch({ type: APP_LOAD, token, skipTracking: true }),
  onRedirect: () =>
    dispatch({ type: REDIRECT }),
});

class App extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      store.dispatch(push(nextProps.redirectTo));
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    const token = window.localStorage.getItem('accessToken');
    if (token) {
      agent.setToken(token);

      // set up token in reducer if exists in localstorage
      // On load send a request to server to verify if has token credentials
      this.props.onLoad(token);
    }
  }

  render() {

    const { inProgress, token } = this.props;

    return (
      <div className="App">
        <Header token={token}/>
        {inProgress && <Loading ></Loading>}
        <main className='container'>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/login" component={SignIn} />
            <Route path="/register" component={Register} />
          </Switch>
        </main>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
