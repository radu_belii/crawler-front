import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText'

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

function InfoCard(props) {
  const { classes, messages } = props;
  const messageList = messages.data.map((message) =>
    <ListItem
      key={message._id}>
      Name: {message.name}
      <br/>
      Message: {message.text}
    </ListItem>);

  //   const messageList = (<List>
  //       <
  // <List/>)

  console.log(messages, 'messages')
  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          message
        </Typography>
        <List>
          {messageList}
        </List>

        <Typography component="p">

        </Typography>
      </CardContent>

      <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions>
    </Card>
  );
}

export default withStyles(styles)(InfoCard);