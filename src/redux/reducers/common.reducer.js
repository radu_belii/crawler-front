import {
  APP_LOAD,
  REDIRECT,
  LOGOUT,
  LOGIN,
  REGISTER,
  INSERT_TOKEN,
} from '../../constants/actionTypes';

const defaultState = {
  token: null,
  isLogIn: false,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_LOAD:
      return {
        ...state,
        token: action.token || null,
        appLoaded: true,
      };

    case INSERT_TOKEN:
    return {
      ...state,
      token: action.payload || null,
    };
    case REDIRECT:
      return { ...state, redirectTo: null };
    case LOGIN:
    case REGISTER:
      return {
        ...state,
        redirectTo: action.error ? null : '/',
        currentUser: action.error ? null : action.payload.user
      };

    case LOGOUT:
      return { ...state, redirectTo: '/', token: null };
    default:
      return state;
  }
};
