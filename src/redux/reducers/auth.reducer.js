import { 
  LOGIN,
  REGISTER,
  ASYNC_START,
  UPDATE_FIELD_AUTH,
  REQUEST_ABOUT,
  LOGIN_PAGE_UNLOADED,
  REGISTER_PAGE_UNLOADED,
  SUCCES_REGISTER,
} from '../../constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        inProgress: false,
        error: action.error ? action.error : null,
      }
    case LOGIN_PAGE_UNLOADED:
    case REGISTER_PAGE_UNLOADED:
      return {};
    case SUCCES_REGISTER:
    debugger;
    return {
      ...state,
      inProgress: false,
      userRegisterData: action.payload,
      redirectTo:'/',
    }
    case ASYNC_START:
      if (action.subtype === LOGIN || action.subtype === REGISTER) {
        return { ...state, inProgress: true };
      }
    break;
    case UPDATE_FIELD_AUTH:
      return { ...state, [action.key]: action.value };
    case REQUEST_ABOUT:
    return { ...state, about: action.payload };
    default:
      return state;
  }
  
  return state;
};