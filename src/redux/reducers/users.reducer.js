import {
  GET_USERS,
  GET_MESSAGES,
  ASYNC_START,
} from '../../constants/actionTypes';


export default (state = {}, action) => {
  switch (action.type) {
    case GET_USERS:
    return {
      ...state,
      inProgress: false,
      users: action.payload.data,
      error: action.error ? action.error : null,
    }
    case GET_MESSAGES:
    return {
      ...state,
      inProgress: false,
      messages: action.payload.data,
      error: action.error ? action.error : null,
    }

    case ASYNC_START:
    if (action.subtype === GET_USERS) {
      return { ...state, inProgress: true };
    }
    if (action.subtype === GET_MESSAGES) {
      return { ...state, inProgress: true };
    }
    
    default:
      return state;
  }
};
