import auth from './reducers/auth.reducer';
import common from './reducers/common.reducer';
import users from './reducers/users.reducer';

import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';



export default combineReducers({
  auth,
  common,
  users,
  router: routerReducer
});
